package com.example.workflow.web.rest;

import com.example.workflow.service.RuntimeService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/runtime/workflow-instances")
public class WorkflowController {
    private final RuntimeService runtimeService;

    @PostMapping("/{workflowDefinitionKey}")
    public void startProcess(@PathVariable String workflowDefinitionKey) {
        Map<String, Object> variables = Map.of("key", "value");
        runtimeService.startWorkflowInstanceByKey(workflowDefinitionKey, variables);
    }
}
