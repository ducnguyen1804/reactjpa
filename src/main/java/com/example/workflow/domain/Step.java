package com.example.workflow.domain;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class Step {
    private String name;
    private String description;
    private Task task;
}
