package com.example.workflow.domain;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class Workflow {
    private String name;
    private String description;
}
