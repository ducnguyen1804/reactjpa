package com.example.workflow.service;

import com.example.workflow.domain.Greeting;
import com.example.workflow.repository.GreetingRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class GreetingService {
    private final GreetingRepository greetingRepository;

    public Greeting greet(String name) {
        var greeting = new Greeting("Hello, " + name + "!");
        return greetingRepository.save(greeting);
    }
}
