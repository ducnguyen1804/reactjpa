package com.example.workflow.service;

public interface JobWorker {
    // Xử lý một task
    void doTask(String taskName);
}
