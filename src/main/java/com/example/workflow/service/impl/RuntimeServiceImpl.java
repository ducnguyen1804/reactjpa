package com.example.workflow.service.impl;

import com.example.workflow.command.StartProcessInstanceCmd;
import com.example.workflow.common.CommandExecutor;
import com.example.workflow.service.RuntimeService;
import com.example.workflow.service.WorkflowExecution;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class RuntimeServiceImpl extends CommonEngineServiceImpl<ProcessEngineConfigurationImpl> implements RuntimeService {
    protected CommandExecutor commandExecutor;


    @Override
    public WorkflowExecution startWorkflowInstanceByKey(String processDefinitionKey) {
        return null;
    }

    @Override
    public WorkflowExecution startWorkflowInstanceByKey(String processDefinitionKey, Map<String, Object> variables) {
        return commandExecutor.execute(new StartProcessInstanceCmd<WorkflowExecution>(processDefinitionKey, null, null, variables));
    }

    @Override
    public WorkflowExecution startWorkflowInstanceByKey(String processDefinitionKey, String tenantId) {
        return null;
    }
}
