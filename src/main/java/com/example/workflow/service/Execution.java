package com.example.workflow.service;

public interface Execution {
    String getId();

    boolean isEnded();

    String getWorkflowExecutionId();

    String getParentId();
}
