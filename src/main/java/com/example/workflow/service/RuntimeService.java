package com.example.workflow.service;

import java.util.Map;

public interface RuntimeService {
    WorkflowExecution startWorkflowInstanceByKey(String processDefinitionKey);

    WorkflowExecution startWorkflowInstanceByKey(String processDefinitionKey, Map<String, Object> variables);

    WorkflowExecution startWorkflowInstanceByKey(String processDefinitionKey, String tenantId);
}
