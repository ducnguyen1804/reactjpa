package com.example.workflow.common;

public enum TransactionPropagation {

    REQUIRED, REQUIRES_NEW, NOT_SUPPORTED,

}
