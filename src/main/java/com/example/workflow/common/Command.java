package com.example.workflow.common;

public interface Command<T> {

    T execute(CommandContext commandContext);
}