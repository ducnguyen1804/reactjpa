package com.example.workflow.common;

public abstract class CommonServiceImpl<C> {

    protected C configuration;

    public CommonServiceImpl() {

    }

    public CommonServiceImpl(C configuration) {
        this.configuration = configuration;
    }

    public C getConfiguration() {
        return configuration;
    }
}
